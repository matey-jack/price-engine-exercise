
Run tests with `mvn test` or in your favorite IDE.

Some notes on design
====================

### Functional Programming

As you can see, I used `final` a lot on locals and fields. More locals in very 
small scopes are so obviously `final` that I didn't even mention it. 

Immutable style is good for understandable code and also to avoid concurrency 
bugs.

 
### Dependency Injection 

I stayed with immutable style, made all injected components `final` and brought 
them in via the constructor. It's readable, self-explaining, easy to test and
if the boiler-plate constructor code becomes too much, it can be replaced with
Lombok's @RequiredArgsConstructor which finds all the finals and builds the 
constructor automatically. (Almost as good as doing it in Kotlin.)

### AutoValue

"AutoValue is a great tool for eliminating the drudgery of writing mundane 
value classes in Java. It encapsulates much of the advice in Effective Java 
Chapter 2, and frees you to concentrate on the more interesting aspects of your
program. The resulting program is likely to be shorter, clearer, and freer of 
bugs. Two thumbs up."
-- Joshua Bloch, author, Effective Java

In this case we only need AutoValue to generate the equals() and hashCode() 
methods for us and (unlike generating them in the IDE) will always keep them
up to date when fields of the class change. This saves work when writing code 
and more importantly, saves a lot of trouble when changing code later.

AutoValue is more portable than Lombok, simpler, and safer. That's why I prefer
using it when I don't need or don't want any of Lombok's additional features.
(Simplest and safest for data classes would be to use Kotlin... it is actually
quite possible to use Kotlin only for some classes and use those classes from
Java code... but I digress ;-)

### Number of classes / very small classes

Java classes in my solution are very small. This is obvious for data/value 
classes like Product and PriceRule. But Checkout and PriceEngine could as well 
be merged into a single class. I just separated them, because the Kata asked 
for decoupling and it was possible to have separate classes and a meaningful 
interface between them. 

Product class is of course not necessary for the solution. It mainly makes 
types of generic maps and function parameter easier to read and less likely to 
confuse which becomes important in bigger programs when there are many strings
in method signatures. 

### Singleton Values to make PriceRule lookup safer

This is something I did _not_ implement, because it would be too complex for
the present example, but I want to mention it as a possible solution for the 
problem of exceptions during scan() and total(). As explained before, it is 
more user-friendly and robust if a not-priced product throws an exception in 
scan() and we can still get a total() for all other products. We do this in the
current code (with the check in Checkout.scan()), but we still need to catch 
the error again in PriceEngine.getPrice() although it can't occur when called
from Checkout.total(). 

If the application were more complex and we had a risk of missing some of those
checks, we could build a check into the factory method of Product itself! Then
we'd only have Product instances which definitely have a price, so we can use
the type system to make sure that exceptions are not thrown deeper down in the
code.

As a side effect, we could cache the instances to have only one live Product 
instance per productId and then use more efficient reference equality to 
compare them. 

But as stated above, it would be too complex for this use-case and also 
introduce too much coupling between Product and PriceEngine.



##### Closing words: thinking out of the box

The best solution for bundle prices on a business level (which also saves Cashier time)
is to only apply bundle prices for products physically bundled together and with a single
price tag.

For inspiration: [A Parable by Edsger W.Dijkstra](http://www.cs.virginia.edu/~evans/cs655/readings/ewd594.html)