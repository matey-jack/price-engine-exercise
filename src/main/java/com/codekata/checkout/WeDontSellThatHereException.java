package com.codekata.checkout;

public class WeDontSellThatHereException extends RuntimeException {
    public WeDontSellThatHereException(Product product) {
        super("Product '" + product.getId() + "' is not sold here.");
    }
}
