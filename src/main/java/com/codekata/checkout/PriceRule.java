package com.codekata.checkout;

import java.util.TreeMap;

public class PriceRule {
    private final int basePrice;

    private final TreeMap<Integer, Integer> bundlePrices;

    PriceRule(int basePrice, TreeMap<Integer, Integer> bundlePrices) {
        this.basePrice = basePrice;
        this.bundlePrices = bundlePrices;
    }

    public int priceFor(int count) {
        int total = 0;
        for (int bundleSize : bundlePrices.navigableKeySet().descendingSet()) {
            int bundlePrice = bundlePrices.get(bundleSize);
            total += (count / bundleSize) * bundlePrice;
            count %= bundleSize;
        }
        return total + count * basePrice;
    }
}
