package com.codekata.checkout;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class Product {
    public abstract String getId();

    public static Product create(String id) {
        return new AutoValue_Product(id);
    }
}
