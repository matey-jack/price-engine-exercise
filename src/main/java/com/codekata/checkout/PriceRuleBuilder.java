package com.codekata.checkout;

import java.util.TreeMap;

public class PriceRuleBuilder {
    private final int basePrice;
    private final TreeMap<Integer, Integer> bundlePrices = new TreeMap<>();

    public PriceRuleBuilder(int basePrice) {
        this.basePrice = basePrice;
    }

    PriceRuleBuilder bundle(int count, int price) {
        bundlePrices.put(count, price);
        return this;
    }

    PriceRuleBuilder getOneFreeFor(int count) {
        bundlePrices.put(count + 1, basePrice * count);
        return this;
    }

    PriceRule build() {
        return new PriceRule(basePrice, bundlePrices);
    }
}
