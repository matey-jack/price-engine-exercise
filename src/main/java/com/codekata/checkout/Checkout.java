package com.codekata.checkout;

import java.util.HashMap;
import java.util.Map;

public class Checkout {
    private final PriceEngine priceEngine;

    // invariant: all products in here are also contained in priceEngine.prices map.
    private final Map<Product, Integer> counts = new HashMap<>();

    public Checkout(PriceEngine priceEngine) {
        this.priceEngine = priceEngine;
    }

    public void scan(Product product) {
        if (!priceEngine.hasPrice(product)) {
            throw new WeDontSellThatHereException(product);
        }
        int oldCount = counts.getOrDefault(product, 0);
        counts.put(product, oldCount + 1);
    }

    public int total() {
        return counts.entrySet().stream().mapToInt(
                pair -> priceEngine.getPrice(pair.getKey(), pair.getValue())
        ).sum();
    }
}
