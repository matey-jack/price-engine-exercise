package com.codekata.checkout;

import java.util.Map;

public class PriceEngine {
    final Map<Product, PriceRule> prices;

    public PriceEngine(Map<Product, PriceRule> prices) {
        this.prices = prices;
    }

    public boolean hasPrice(Product product) {
        return prices.containsKey(product);
    }

    /**
     * @param product - the product to price, PRECONDITION: hasPrice(product) must be true!
     * @param count - how many of {@code product} to get a price for
     * @return price of {@code count} items in cents
     */
    public int getPrice(Product product, int count) {
        PriceRule p = prices.get(product);
        if (p == null) {
            throw new IllegalArgumentException(product.getId() + " has no price.");
        }
        return p.priceFor(count);
    }
}
