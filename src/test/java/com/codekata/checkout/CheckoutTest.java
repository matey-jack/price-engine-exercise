package com.codekata.checkout;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class CheckoutTest {
    final Map<Product, PriceRule> prices = new HashMap<>();

    @Before
    public void fillPrices() {
        prices.put(Product.create("A"), new PriceRuleBuilder(50).bundle(3, 130).build());
        prices.put(Product.create("B"), new PriceRuleBuilder(30).bundle(2, 45).build());
        prices.put(Product.create("C"), new PriceRuleBuilder(20).build());
        prices.put(Product.create("D"), new PriceRuleBuilder(15).build());
    }

    int price(String goods) {
        final Checkout co = new Checkout(new PriceEngine(prices));
        for (String product : goods.split(" ")) {
            if (product.isEmpty()) continue;
            co.scan(Product.create(product));
        }
        return co.total();
    }

    @Test
    public void test_totals() {
        assertEquals(0, price(""));
        assertEquals(50, price("A"));
        assertEquals(80, price("A B"));
        assertEquals(115, price("C D B A"));
        assertEquals(100, price("A A"));
        assertEquals(130, price("A A A"));
        assertEquals(180, price("A A A A"));
        assertEquals(230, price("A A A A A"));
        assertEquals(260, price("A A A A A A"));
        assertEquals(160, price("A A A B"));
        assertEquals(175, price("A A A B B"));
        assertEquals(190, price("A A A B B D"));
        assertEquals(190, price("D A B A B A"));
    }

    @Test
    public void test_incremental() {
        final Checkout co = new Checkout(new PriceEngine(prices));
        final Product a = Product.create("A");
        final Product b = Product.create("B");

        assertEquals(0, co.total());

        co.scan(a);
        assertEquals(50, co.total());

        co.scan(b);
        assertEquals(80, co.total());

        co.scan(a);
        assertEquals(130, co.total());

        co.scan(a);
        assertEquals(160, co.total());

        co.scan(b);
        assertEquals(175, co.total());
    }

    @Test
    public void buyTwoGetThree_sinceOneIsFree() {
        final Map<Product, PriceRule> myPrices = new HashMap<>();
        final Product coke = Product.create("Coke");
        final Product chips = Product.create("Chips");

        // we don't have prices for all products!
        myPrices.put(coke, new PriceRuleBuilder(50).build());

        final Checkout co = new Checkout(new PriceEngine(myPrices));

        co.scan(coke);
        try {
            co.scan(chips);
        } catch (WeDontSellThatHereException e) {
            // ignore this here
        }
        assertEquals(50, co.total());
    }

    @Test
    public void getTotalWhenOneProductNotFound() {
        final Map<Product, PriceRule> myPrices = new HashMap<>();
        final Product coke = Product.create("Coke");
        // use the convenience builder
        myPrices.put(coke, new PriceRuleBuilder(50).getOneFreeFor(2).build());

        final Checkout co = new Checkout(new PriceEngine(myPrices));

        co.scan(coke);
    }
}