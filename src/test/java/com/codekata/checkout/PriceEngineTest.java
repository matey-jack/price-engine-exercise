package com.codekata.checkout;

import org.junit.Test;

import static org.junit.Assert.*;

public class PriceEngineTest {
    @Test
    public void testManyBundles() {
        PriceRule p = new PriceRuleBuilder(100)
                .bundle(3, 220)
                .bundle(10, 890)
                .build();
        assertEquals(100, p.priceFor(1));
        assertEquals(320, p.priceFor(4));
        assertEquals(890+320, p.priceFor(14));
        assertEquals(8900 + 440 + 200, p.priceFor(108));
    }
}